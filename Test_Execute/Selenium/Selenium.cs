using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace Test_Execute.Selenium
{

    class Selenium
    {
        static String[] parts; // variable para almacenar las cadenas desintegradas
        static String[] part; // variable para almacenar las cadenas desintegradas
        static private String[,] testInactivos = new String[499, 1];
        static private int iteracionTest = 0;
        //get y set para la vairable testInactivos
        public String[,] TestInactivos
        {
            get
            {
                return testInactivos;
            }
            set
            {
                testInactivos = value;
            }
        }
        //iterador de test Inactivos
        public int IteracionTest
        {
            get
            {
                return iteracionTest;
            }
            set
            {
                iteracionTest = value;
            }
        }

        public void Navegar(ChromeDriver driver, String URL, List<String> list, String ID_TFS)
        {
            try
            {
                driver.Manage().Window.Maximize();
                driver.Navigate().GoToUrl(URL);
            }
            catch
            {
            }
        }
        public void clean(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Id(NameObject)).Clear();
                            list.Add("Pass,Clear al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }

                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "Name":
                    try
                    {
                        Thread.Sleep(1000);
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            driver.FindElement(By.Name(NameObject)).Clear();
                            list.Add("Pass,Clear al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "XPath":
                    try
                    {
                        Thread.Sleep(1000);
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            driver.FindElement(By.XPath(NameObject)).Clear();
                            list.Add("Pass,Clear al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
            }
        }
        public void SetTextVal(ChromeDriver driver, String TypeElement, String NameObject, String ValObeject, List<String> list, String ID_TFS)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Id(NameObject)).SendKeys(ValObeject);
                        }

                    }
                    catch
                    {
                    }
                    break;
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Name(NameObject)).SendKeys(ValObeject);
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Enabled)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.XPath(NameObject)).SendKeys(ValObeject);
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
            }

        }
        public void SetTextValClear(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Id(NameObject)).Clear();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Name(NameObject)).Clear();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.XPath(NameObject)).Clear();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
            }

        }
        public void ClickButton(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS, int tiempo)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Id(NameObject)).Click();
                        }

                    }
                    catch
                    {
                    }
                    break;
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Name(NameObject)).Click();
                            list.Add("Pass,Click al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(tiempo));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {

                            driver.FindElement(By.XPath(NameObject)).Click();
                            list.Add("Pass,Click al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "LinkText":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                        var myElement = wait.Until(x => x.FindElement(By.LinkText(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(3000);
                            driver.FindElement(By.XPath(NameObject)).Click();
                            Thread.Sleep(1000);
                            list.Add("Pass,Click al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "Value":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(30));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(3000);
                            driver.FindElement(By.XPath(NameObject)).Click();
                            Thread.Sleep(1000);
                            list.Add("Pass,Click al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
            }
        }
        public bool Validate_message(ChromeDriver driver, String TypeElement, String NameObject, String ValObject, List<String> list, String ID_TFS)
        {
            string TextObject;

            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            TextObject = myElement.Text;

                            if (TextObject.Equals(ValObject))
                            {
                                list.Add("Pass,Se valido el mensaje " + TextObject + "," + ID_TFS);
                                return true;
                            }
                            else
                            {
                                list.Add("Failed,Se esperaba otro mensaje ," + ID_TFS);
                                return false;
                            }
                        }
                        else
                        {
                            list.Add("Failed,No se encontro el Mensaje ," + ID_TFS);
                            return false;
                        }

                    }
                    catch
                    {
                        return false;
                    }

                case "name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            TextObject = myElement.Text;

                            if (TextObject.Equals(ValObject))
                            {
                                list.Add("Pass,Se valido el mensaje " + TextObject + "," + ID_TFS);
                                return true;
                            }
                            else
                            {
                                list.Add("Failed,Se esperaba otro mensaje ," + ID_TFS);
                                return false;
                            }
                        }
                        else
                        {
                            list.Add("Failed,No se encontro el Mensaje ," + ID_TFS);
                            return false;
                        }

                    }
                    catch
                    {
                        return false;
                    }

                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            TextObject = myElement.Text;

                            if (TextObject.Equals(ValObject))
                            {
                                list.Add("Pass,Se valido el mensaje " + TextObject + "," + ID_TFS);
                                return true;
                            }
                            else
                            {
                                list.Add("Failed,Se esperaba otro mensaje ," + ID_TFS);
                                return false;
                            }
                        }
                        else
                        {
                            list.Add("Failed,No se encontro el Mensaje ," + ID_TFS);
                            return false;
                        }

                    }
                    catch
                    {
                        return false;
                    }

                default:
                    return false;
            }
        }
        public void Validar_Objetos_home(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS)
        {
            try
            {
                var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                if (myElement.Displayed)
                {
                    string objeto1 = driver.FindElement(By.XPath(NameObject + "/a[1]")).Text;
                    string objeto2 = driver.FindElement(By.XPath(NameObject + "/a[2]")).Text;
                    string objeto3 = driver.FindElement(By.XPath(NameObject + "/a[3]")).Text;
                    if (objeto1.Equals("Sales"))
                    {
                        if (objeto2.Equals("Website"))
                        {
                            if (objeto3.Equals("Helpdesk"))
                            {
                                list.Add("Pass,Se encuetra los objetos correspondientes ," + ID_TFS);
                            }
                        }
                    }
                    else
                    {
                        list.Add("Failed,No se encontro el orden correcto ," + ID_TFS);
                    }
                }
                else
                {
                    list.Add("Failed,No se localizaron objetos ," + ID_TFS);
                }
            }
            catch
            {
                list.Add("Failed,No se encontraron objetos ," + ID_TFS);
            }
        }
        public void Senkeys(ChromeDriver driver, String keys, int ciclo)
        {
            int index = 0;
            switch (keys)
            {

                case "Down":
                    {
                        do
                        {
                            driver.Keyboard.SendKeys(Keys.Down);
                            Thread.Sleep(500);
                            index++;
                        } while (index < ciclo);
                    }
                    break;
                case "Enter":
                    {
                        do
                        {
                            driver.Keyboard.SendKeys(Keys.Enter);
                            Thread.Sleep(500);
                            index++;
                        } while (index < ciclo);
                    }
                    break;
                case "Up":
                    {
                        do
                        {
                            driver.Keyboard.SendKeys(Keys.Up);
                            Thread.Sleep(500);
                            index++;
                        } while (index < ciclo);
                    }
                    break;
                case "PageDown":
                    {
                        do
                        {

                            driver.Keyboard.SendKeys(Keys.PageDown);
                            Thread.Sleep(500);
                            index++;
                        } while (index < ciclo);
                    }
                    break;
                case "F5":
                    {
                        do
                        {
                            driver.Navigate().Refresh();
                            Thread.Sleep(500);
                            index++;
                        } while (index < ciclo);
                    }
                    break;
            }
        }
        public string GetData(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS)
        {
            string valor = "";
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            valor = driver.FindElement(By.Id(NameObject)).Text;
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(25));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            valor = driver.FindElement(By.Name(NameObject)).Text;
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(25));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(3000);
                            valor = driver.FindElement(By.XPath(NameObject)).Text;
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
            }
            return valor;
        }
        public string GetAttribute(ChromeDriver driver, String TypeElement, String NameObject, string Attribute, List<String> list, String ID_TFS)
        {
            string valor = "";
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            valor = driver.FindElement(By.Id(NameObject)).GetAttribute(Attribute);
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            valor = driver.FindElement(By.Name(NameObject)).GetAttribute(Attribute);
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(3000);
                            valor = driver.FindElement(By.XPath(NameObject)).GetAttribute(Attribute);
                            list.Add("Pass,Se obtuvo el valor del objeto " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                            return valor;
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                        return valor;
                    }
            }
            return valor;
        }
        public void Wait(int Second)
        {
            Thread.Sleep(Second * 1000);
        }
        public void Waitminutes(int Minutes)
        {
            Thread.Sleep(Minutes * 60000);
        }
        public void Mouse(ChromeDriver driver, String NameObject)
        {
            Actions move = new Actions(driver);

            var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(15));
            IWebElement myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));

            move.MoveByOffset(444, 555).Click();
        }
        public Boolean ExistElement(ChromeDriver driver, String TypeElement, String NameObject, List<String> list, String ID_TFS)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }

                case "name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }

                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    catch
                    {
                        return false;
                    }

                default:
                    return false;
            }
        }
        public void SelectValueText(ChromeDriver driver, String TypeElement, String NameObject, string OpcionText, List<String> list, String ID_TFS)
        {
            switch (TypeElement)
            {
                case "Id":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Id(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Id(NameObject)).FindElement(By.Id($"{NameObject}/option[contains(text(),'{OpcionText}')]")).Click();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "Name":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.Name(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.Name(NameObject)).FindElement(By.Name($"{NameObject}/option[contains(text(),'{OpcionText}')]")).Click();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
                case "XPath":
                    try
                    {
                        var wait = new WebDriverWait(driver, TimeSpan.FromSeconds(10));
                        var myElement = wait.Until(x => x.FindElement(By.XPath(NameObject)));
                        if (myElement.Displayed)
                        {
                            Thread.Sleep(1000);
                            driver.FindElement(By.XPath(NameObject)).FindElement(By.XPath($"{NameObject}/option[contains(text(),'{OpcionText}')]")).Click();
                            list.Add("Pass,SetTex al objeto " + NameObject + "," + ID_TFS);
                        }
                        else
                        {
                            list.Add("Failed,No se a encontrado el objeto : " + NameObject + "," + ID_TFS);
                        }
                    }
                    catch
                    {
                        list.Add("Failed,Error en el objeto " + NameObject + "," + ID_TFS);
                    }
                    break;
            }

        }

        public void scrooll(ChromeDriver driver, String xpath)
        {

            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            IWebElement Element = (IWebElement)driver.FindElement(By.XPath(xpath));
            js.ExecuteScript("arguments[0].scrollIntoView();", Element);
            Thread.Sleep(1000);
        }
        public void ExistWhitParameter(ChromeDriver driver, string element, int a)
        {

            Thread.Sleep(a);
            IWebElement myLink = driver.FindElementByXPath(element);
        }
        public void Equals(ChromeDriver driver, string element, String text1)
        {
            String text2 = driver.FindElement(By.XPath(element)).Text;
            Assert.AreEqual(text1, text2);
        }
        public void cargando(ChromeDriver driver, String xpath)
        {
            try
            {
                driver.FindElement(By.XPath(xpath));
                cargando(driver, xpath);
            }
            catch (Exception e)
            {
                Console.WriteLine("Ha pasado con exito \n");
            }

        }
        public void SendKeysTab(ChromeDriver driver, string xpath, string text)
        {

            Thread.Sleep(2000);
            IWebElement webElement = driver.FindElementByXPath(xpath);
            webElement.SendKeys(text);
            webElement.SendKeys(Keys.Tab);
        }
        //funcion para desintegrar texto
        public String[] SeparaTexto(String texto)
        {
            parts = texto.Split(',');
            return parts;
        }
        //cambiar Extencion de Arhivo
        //cambiar Extencion de Arhivo
        public void renombrararchivo(String DirectorioPrincipal, String DirectorioTest, String extención)
        {
            String extenciónActual;
            String nombreTest = DirectorioTest;
            if (extención == ".txt")
            {
                extenciónActual = ".cs";
            }
            else
            {
                extenciónActual = ".txt";
            }
            //Crear Dirección de archivo a Renombrar
            var path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");

            var pathFinal = path.Replace("bin\\Debug\\.\\", "") + "Test_BackOffice\\Usuarios\\" + DirectorioPrincipal + "\\" + DirectorioTest + "\\" + nombreTest + "-SP" + extenciónActual;

            Console.WriteLine("DirecciónTestDesactivado" + pathFinal);
            FileInfo f = new FileInfo(pathFinal);
            f.MoveTo(Path.ChangeExtension(pathFinal, extención));
            Console.WriteLine("\nArchivo Modificado " + pathFinal);
            //Regresar a su forma Normal;
            if (extención == ".txt")
            {
                conversion(DirectorioPrincipal, DirectorioTest);
            }
        }
        //Generar una excepcion
        public bool detenerPrueba()
        {
            try
            {
                //  float a = 5/0;
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }
        public void conversion(String directoriPrincipal, String directorioTest)
        {
            var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");

            //SP significa Clase super Padre
            var pathFinal = jsonFolder.Replace("bin\\Debug\\.\\", "") + "Test_BackOffice\\Usuarios\\" + directoriPrincipal + "\\" + directorioTest + "\\" + directorioTest + "-SP";

            using (StreamWriter fileWrite = new StreamWriter(pathFinal + "temp.txt"))
            {
                using (StreamReader fielRead = new StreamReader(pathFinal + ".txt"))
                {
                    String line;
                    Console.WriteLine("\n" + "**********************************************");
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        Console.WriteLine("\n" + line);
                        String datos = line.Replace(" ", String.Empty);
                        if (datos != "[Test]")
                        {
                            fileWrite.WriteLine(line);
                        }
                        else
                        {
                            fileWrite.WriteLine("       //");
                        }

                    }
                }
            }

            //aqui se renombrea el archivo temporal
            File.Delete(pathFinal + ".txt");
            File.Move(pathFinal + "temp.txt", pathFinal + ".txt");


        }


    }
}

