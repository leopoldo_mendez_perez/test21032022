﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.suit;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test02_Acceso_Usuario_Contraseña_Incorrecta_SupAdmin
    {

        static ChromeDriver driver;
        static Login_Usuario_Contraseña_Incorrecta loginUserPassIncorrect = new Login_Usuario_Contraseña_Incorrecta();
        private static List<String> lista = new List<String>();
        static Config Conf = new Config();

        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        /*
        * CP01-02	Acceder al portal del BackOffice usuario y password incorrecto
        */

        public void Test02_Acceso_Usuario_Contraseña_Incorrecta_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Acceso_Usuario_Contraseña_Incorrecta_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                loginUserPassIncorrect.LoginUserPassowrdError(driver, lista, DatosBO._010201_userError, DatosBO._010202_passwordError);

            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }
    }
}