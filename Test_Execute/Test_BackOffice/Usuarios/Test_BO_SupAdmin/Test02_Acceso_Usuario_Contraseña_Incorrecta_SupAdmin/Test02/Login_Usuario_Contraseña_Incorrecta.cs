﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;
namespace Test_Execute.Selenium.suit
{
    class Login_Usuario_Contraseña_Incorrecta
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";

        string Orden;
        static login login = new login();

        /*
      * Fecha: 21.02.22
      * Creador: Leopoldo QA
      * Objetivo: Método que valida el error Passoword
      * Ultima actualización:
      * Responsable Última actualización:
      * Objetivo última actualización:
      */
        public void LoginUserPassowrdError(ChromeDriver driver, List<String> lista, string UserName, string PassUser)
        {
            try
            {
                login.loginFrontClaro(driver, lista, UserName, PassUser);
                //Validacion del texto de error
                Functions.Equals(driver, LocalizadoresBO._01023_textUser, DatosBO._010203_user_error_text);
                Functions.Equals(driver, LocalizadoresBO._01022_textPassword, DatosBO._010204_password_error_text);
            }
            catch (Exception e)
            {
                // Assert.Fail Se genera el error y se detiene el test
                Assert.Fail("Error " + e);
                //Si hay algun error entonces que se quite el chromeDriver en las tareas del sistema
                driver.Quit();

            }
            driver.Quit();
        }
    }
}
