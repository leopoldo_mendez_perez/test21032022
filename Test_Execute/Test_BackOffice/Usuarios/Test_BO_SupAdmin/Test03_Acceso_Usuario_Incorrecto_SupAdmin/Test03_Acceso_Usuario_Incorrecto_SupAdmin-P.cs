﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.suit;
using Test_Inactivos;
namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test03_Acceso_Usuario_Incorrecto_SupAdmin
    {
        static ChromeDriver driver;
        static Config Conf = new Config();
        static Selenium Functions = new Selenium();
        static Login_con_usuario_Incorrecto loginUsuarioIncorrecto = new Login_con_usuario_Incorrecto();
        private static List<String> lista = new List<String>();
        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        /*
        * CP01-03	Acceder al portal del BackOffice usuario incorrecto
        */
        public void Test03_Acceso_Usuario_Incorrecto_SupAdmin_()
        {
            if (!lecEstado.LecturaTestInactivos("Acceso_Usuario_Incorrecto_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                loginUsuarioIncorrecto.LoginUserError(driver, lista, DatosBO._010201_userError, DatosBO._010202_passwordError);
                driver.Quit();
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }
    }
}