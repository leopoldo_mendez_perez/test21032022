﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;


namespace Test_Execute.Selenium.suit
{
    class Login_con_usuario_Incorrecto
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";

        string Orden;
        static login login = new login();

        /*
      * Fecha:21.02.22
      * Creador: Leopoldo QA
      * Objetivo: Método de que valida el  error username 
      * Ultima actualización:
      * Responsable Ultima actualización:
      * Objetivo ultima actualización:
      */
        public void LoginUserError(ChromeDriver driver, List<String> lista, string UserName, string PassUser)
        {
            login.loginFrontClaro(driver, lista, UserName, PassUser);
            //Validacion del texto de error
            Functions.Equals(driver, LocalizadoresBO._01023_textUser, DatosBO._010203_user_error_text);

        }
    }
}
