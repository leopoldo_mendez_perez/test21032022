﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.suit;
using Test_Inactivos;

namespace ValidacionInterfacesAuto
{
    [TestFixture]
    public class Test01_Login_Correcto_SupAdmin : Attribute
    {

        Lectura_Test_Inactivos lecEstado = new Lectura_Test_Inactivos();
        static ChromeDriver driver;
        static Config Conf = new Config();
        static login loginCorreto = new login();
        private static List<String> lista = new List<String>();
        /*
    * CP01-01	Acceso al portal BackOffice Correcto
    */
        public void Test01_Login_Correcto_SupAdmin_()
        {

            if (!lecEstado.LecturaTestInactivos("Login_Correcto_SupAdmin"))
            {
                driver = Conf.ConfigChrome();
                loginCorreto.LoginSuccefull(driver, lista, DatosBO._010101_user, DatosBO._010102_password);
                driver.Quit();
            }
            else
            {
                Assert.Ignore("Test ignored during Prod runs");
            }
        }
        // 


    }
}