﻿using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using Test_Execute.Selenium.ResxBO;
using Test_Execute.Selenium.ResxConfGlob;

namespace Test_Execute.Selenium.suit
{
    class login
    {

        Selenium Functions = new Selenium();
        string ruta = @"C:\Logs_TE_TER\";

        string Orden;



        /*
         * Se empiezan con los metodos que se utilizaran en la automatización de los escenarios
         */
        /*
         * Fecha: 21.02.22
         * Creador: Ruben Velazquez Patiño (bSide)
         * Objetivo: Método de login exitoso
         * Ultima actualización:
         * Responsable Última actualización:
         * Objetivo ultima actualización:
         */
        public void loginFrontClaro(ChromeDriver driver, List<String> lista, string UserName, string PassUser)
        {
            Functions.Navegar(driver, Conf.url_BO, lista, "0");
            Functions.SetTextVal(driver, "Name", LocalizadoresBO._01012_boxUserLogin, $"{UserName}", lista, "0");
            Functions.SetTextVal(driver, "Name", LocalizadoresBO._01011_boxPassLogins, PassUser, lista, "0");
            Functions.scrooll(driver, LocalizadoresBO._01013_buttonIngresarLogin);
            Functions.ClickButton(driver, "Value", LocalizadoresBO._01013_buttonIngresarLogin, lista, "0", 0);
            Functions.Wait(3);
        }
        public void LoginSuccefull(ChromeDriver driver, List<String> lista, string UserName, string PassUser)
        {
            loginFrontClaro(driver, lista, UserName, PassUser);
            Functions.cargando(driver, LocalizadoresBO._011005_procesando);
            Functions.ExistWhitParameter(driver, LocalizadoresBO._01014_cerrar_sesion, 2);

        }

    }


}
