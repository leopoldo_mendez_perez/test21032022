using TestSuperAdministradors;
using TestAdministradors;
using TestEmpresa;
using System;
using System.IO;
using System.Threading;

namespace DesactivaciónTestBO
{
    class DesactivacónTestB0
    {
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        SuperAdministrador sup = new SuperAdministrador();
        Administrador adm = new Administrador();
        Empresa emp = new Empresa();
        MultiPais mult = new MultiPais();
        /*
        * Fecha: 21.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Método para la Desactivación de test por medio del estado 0, para usuario SuperAdministrador,Administrador,Empresa
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void DesactivaciónTestB0 ()
        {
            var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");

            var pathFinal = jsonFolder.Replace("bin\\Debug\\.\\", "") + "Test_BackOffice\\Desactivacion_Activacion_BackOffice.txt";
            string FileToRead = pathFinal;
                using (StreamReader ReaderObject = new StreamReader(FileToRead))
                {
                    string Line, textofin, numerofin;
                    //While que lee cada linea de texto
                    while ((Line = ReaderObject.ReadLine()) != null)
                    {
                    Thread.Sleep(500);
                    try {
                        //separación de estado y nombre de test
                        textofin = (Line.Substring(2));
                        numerofin = (Line.Substring(0, 1));
                        Console.WriteLine("Texto: "+textofin+"\n"+"Numero: "+numerofin);
                        //funcion para desactivación de test
                     sup.testSuperAdministrador(textofin, numerofin);
                        adm.testAdministrador(textofin, numerofin);
                        emp.testEmpresa(textofin, numerofin);
                        mult.testMultiPais(textofin, numerofin);

                    }
                    catch (Exception e) {
                        
                    }



                }
                }
           
        }

       
    }
}
