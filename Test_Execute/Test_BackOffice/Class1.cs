﻿using System;
using System.IO;

namespace Test_Execute.Test_BackOffice
{
    internal class Class1
    {
        //  [Test]
        public void conversion()
        {
            var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");


            var pathFinal = jsonFolder.Replace("\\bin\\Debug\\.\\", "") + "\\Test_BackOffice\\Test01_Login_Correcto_SupAdmin";


            using (StreamWriter fileWrite = new StreamWriter(pathFinal + "temp.txt"))
            {
                using (StreamReader fielRead = new StreamReader(pathFinal + ".txt"))
                {
                    String line;
                    Console.WriteLine("\n" + "**********************************************");
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        Console.WriteLine("\n" + line);
                        String datos = line.Replace(" ", String.Empty);
                        if (datos != "[Test]")
                        {
                            fileWrite.WriteLine(line);
                        }
                        else
                        {
                            fileWrite.WriteLine("       //");
                        }

                    }
                }
            }

            //aqui se renombrea el archivo temporal
            File.Delete(pathFinal + ".txt");
            File.Move(pathFinal + "temp.txt", pathFinal + ".txt");


        }
    }
}
