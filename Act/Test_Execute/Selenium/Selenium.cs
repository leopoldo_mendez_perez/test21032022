
using System;
using System.IO;
using Test_Execute.Selenium.ResxConfGlob;
namespace Test_Execute.Selenium
{

    class Selenium
    {
        static String[] parts; // variable para almacenar las cadenas desintegradas
        static String[] part; // variable para almacenar las cadenas desintegradas
        static private String[,] testInactivos = new String[499, 1];
        static private int iteracionTest=0;
       //get y set para la vairable testInactivos
        public String [,] TestInactivos
        {
            get {
                return testInactivos;
                    }
            set
            {
                testInactivos = value;
            }
        }
        //iterador de test Inactivos
        public int IteracionTest
        {
            get
            {
                return iteracionTest;
            }
            set
            {
                iteracionTest = value;
            }
        }

        //funcion para desintegrar texto
        public String[] SeparaTexto(String texto)
        {
            parts = texto.Split(',');
            return parts;
        }
        //cambiar Extencion de Arhivo
        public void renombrararchivo(String DirectorioPrincipal, String DirectorioTest, String extención)
        {
            String extenciónActual;
            String nombreTest = DirectorioTest;
            if (extención == ".txt")
            {
                extenciónActual = ".cs";
            }
            else
            {
                extenciónActual = ".txt";
            }
            //Crear Dirección de archivo a Renombrar
            var path = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");

            var pathFinal = path.Replace("Act\\Test_Execute\\bin\\" + Conf.tipoEjecucion + "\\.\\", "") + "Test_Execute\\Test_BackOffice\\Usuarios\\" + DirectorioPrincipal + "\\" + DirectorioTest + "\\" + nombreTest + "-SP" + extenciónActual;

            Console.WriteLine("DirecciónTestDesactivado" + pathFinal);
            FileInfo f = new FileInfo(pathFinal);
            f.MoveTo(Path.ChangeExtension(pathFinal, extención));
            Console.WriteLine("\nArchivo Modificado " + pathFinal);
            //Regresar a su forma Normal;
            if (extención == ".txt")
            {
                conversion(DirectorioPrincipal, DirectorioTest);
            }
        }
        //Generar una excepcion
        public bool detenerPrueba() {
            try {
              //  float a = 5/0;
                return true;
            }
            catch (Exception e) {
                return false;
            }
           
        }
        public void conversion(String directoriPrincipal,String directorioTest)
        {
            var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");

            //SP significa Clase super Padre
            var pathFinal = jsonFolder.Replace("Act\\Test_Execute\\bin\\" + Conf.tipoEjecucion + "\\.\\", "") + "Test_Execute\\Test_BackOffice\\Usuarios\\" + directoriPrincipal + "\\"+ directorioTest+"\\" + directorioTest+"-SP";


            using (StreamWriter fileWrite = new StreamWriter(pathFinal + "temp.txt"))
            {
                using (StreamReader fielRead = new StreamReader(pathFinal + ".txt"))
                {
                    String line;
                    Console.WriteLine("\n" + "**********************************************");
                    while ((line = fielRead.ReadLine()) != null)
                    {
                        Console.WriteLine("\n" + line);
                        String datos = line.Replace(" ", String.Empty);
                        if (datos != "[Test]")
                        {
                            fileWrite.WriteLine(line);
                        }
                        else
                        {
                            fileWrite.WriteLine("       //");
                        }

                    }
                }
            }

            //aqui se renombrea el archivo temporal
            File.Delete(pathFinal + ".txt");
            File.Move(pathFinal + "temp.txt", pathFinal + ".txt");


        }
        public void DeleteDirectory(string path)
        {

            try
            {

                string[] files = Directory.GetFiles(path);
                string[] dirs = Directory.GetDirectories(path);
                //Eliminación de archivos dentro el directorio
                foreach (string file in files)
                {
                    File.SetAttributes(file, FileAttributes.Normal);
                    File.Delete(file);
                }
                //Eliminación de directorios contenidos
                foreach (string dir in dirs)
                {
                    DeleteDirectory(dir);
                }
                //Eliminación de archivos
                Directory.Delete(path, false);

            }
            catch (Exception e)
            {

            }
        }



    }
}

