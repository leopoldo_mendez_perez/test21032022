using TestSuperAdministradors;
using TestAdministradors;
using TestEmpresa;
using System;
using System.IO;
using System.Threading;
using Test_Execute.Selenium.ResxConfGlob;
using Test_Execute.Selenium;
namespace DesactivaciónTestBO
{
    class DesactivacónTestB0
    {
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        SuperAdministrador sup = new SuperAdministrador();
        Administrador adm = new Administrador();
        Empresa emp = new Empresa();
        MultiPais mult = new MultiPais();
        Selenium Functions = new Selenium();
        /*
        * Fecha: 21.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Método para la Desactivación de test por medio del estado 0, para usuario SuperAdministrador,Administrador,Empresa
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void DesactivaciónTestB0 ()
        {
            var jsonFolder = System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @".\\");
            Console.WriteLine("Direccion1\n" + jsonFolder);

            var pathFinal = jsonFolder.Replace("Act\\Test_Execute\\bin\\" + Conf.tipoEjecucion + "\\.\\", "") + "Test_Execute\\Test_BackOffice\\Desactivacion_Activacion_BackOffice.txt";
            string FileToRead = pathFinal;
            Console.WriteLine("Direccion2\n"+pathFinal);

                
                using (StreamReader ReaderObject = new StreamReader(FileToRead))
                {
                    string Line, textofin, numerofin;
                    //While que lee cada linea de texto
                    while ((Line = ReaderObject.ReadLine()) != null)
                    {
                    Thread.Sleep(500);
                    try {
                        //separación de estado y nombre de test
                        textofin = (Line.Substring(2));
                        numerofin = (Line.Substring(0, 1));
                        Console.WriteLine("Texto: "+textofin+"\n"+"Numero: "+numerofin);
                        //funcion para desactivación de test
                     sup.testSuperAdministrador(textofin, numerofin);
                        adm.testAdministrador(textofin, numerofin);
                        emp.testEmpresa(textofin, numerofin);
                        mult.testMultiPais(textofin, numerofin);

                    }
                    catch (Exception e) {
                        
                    }



                }
                //Eleiminacion de configuración actual devops

                //SP significa Clase super Padre
                var pathFinalVS = jsonFolder.Replace("Act\\Test_Execute\\bin\\" + Conf.tipoEjecucion + "\\.\\", "") + ".vs";
                Console.WriteLine("Files deleted successfully" + pathFinalVS);
                Functions.DeleteDirectory(pathFinalVS);

                var pathFinalDebug = jsonFolder.Replace("Act\\Test_Execute\\bin\\" + Conf.tipoEjecucion + "\\.\\", "") + "Test_Execute\\bin\\Debug" + Conf.tipoEjecucion;
                Console.WriteLine("Files deleted successfully" + pathFinalDebug);
                Functions.DeleteDirectory(pathFinalDebug);

            }
      
        }

       
    }
}
