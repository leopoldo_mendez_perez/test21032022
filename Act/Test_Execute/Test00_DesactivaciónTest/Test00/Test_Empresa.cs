﻿using System;
using System.IO;
using Test_Execute.Selenium;

namespace TestEmpresa
{
    class Empresa
    {
        string ruta = @"C:\Logs_TE_TER\";
        string Orden;
        static String[] part; // variable para almacenar las cadenas desintegradas
        static Selenium Functions = new Selenium();
        /*
        * Fecha: 22.02.22
        * Creador: Leopoldo Mendez Perez (QA)
        * Objetivo: Desactivación de test Dependiendo el estado, si lo que se recive es 0 desactivacion/1 Acivacipón
        * Ultima actualización:
        * Responsable Ultima actualización:
        * Objetivo ultima actualización:
        */
        public void testEmpresa(String textofin, String numerofin)
        {
            String directorioPadre = "Test_BO_Empresa";
            switch (textofin)
            {
                case "Crear_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test19_Crear_Usuario_Empresa", ".txt") ;
                        Console.WriteLine("Se Desactivo Test"+ textofin+"\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Buscar_Usuario_Empresa_Creado":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test20_Buscar_Usuario_Empresa_Creado", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Visualizar_datos_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test21_Visualizar_datos_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Cambio_Contraseña_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test22_Cambio_Contraseña_Usuario_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;

                    }
                    break;
                case "Asociación_Lineas_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test23_Asociación_Lineas_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Mensaje_Linea_Asociada_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test24_Validación_Mensaje_Linea_Asociada_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Desasociación_Linea_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test25_Desasociación_Linea_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Edición_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test26_Edición_Usuario_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Busqueda_Usuario_Empresa_Editado":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test27_Busqueda_Usuario_Empresa_Editado", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Edición_Rol_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test28_Edición_Rol_Usuario_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Validación_Edicion_Rol_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test29_Validación_Rol_Editado_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
                case "Eliminación_Usuario_Empresa":
                    if (numerofin == "0")
                    {
                        Functions.renombrararchivo(directorioPadre, "Test30_Eliminación_Usuario_Empresa", ".txt");
                        Console.WriteLine("Se Desactivo Test" + textofin + "\n");
                        Functions.TestInactivos[Functions.IteracionTest, 0] = textofin;
                        Functions.IteracionTest++;
                    }
                    break;
            }
        }
    }
}
